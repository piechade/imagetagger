using System;
using System.Globalization;
using System.Windows.Data;

namespace ImageTagger
{
    /// <summary>
    ///     Converts an exposure time from a decimal (e.g. 0.0125) into a string (e.g. 1/80)
    /// </summary>
    public class ExposureTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var exposure = (decimal)value;

                if (exposure == 0)
                {
                    return String.Empty;
                }
                else
                {
                    exposure = decimal.Round(1 / exposure);
                    return $"1/{exposure}";
                }
            }
            catch (NullReferenceException)
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            var exposure = ((string)value).Substring(2);
            return (1 / decimal.Parse(exposure));
        }
    }
}