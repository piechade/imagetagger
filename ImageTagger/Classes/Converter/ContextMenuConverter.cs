﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace ImageTagger
{
    class ContextMenuConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //Debugger.Break();

            return App.PhotoCollection.Labels;

            //ObservableCollection<ContextItem> items = new ObservableCollection<ContextItem>();
            //for (int i = 0; i < App.PhotoCollection.Labels.Count; i++)
            //{
            //    items.Add(new ContextItem() { Value = App.PhotoCollection.Labels[i].Name });
            //}

            //return items;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //Debugger.Break();
            return value;
        }
    }
}
