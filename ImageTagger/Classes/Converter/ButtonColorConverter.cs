﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace ImageTagger
{
    class ButtonColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //Debugger.Break();

            if (App.PhotoCollection.SelectedPhoto.Label != null)
            {
                if (App.PhotoCollection.SelectedPhoto.Label.Name.Equals(value))
                {
                    return new SolidColorBrush(
                        Color.FromArgb(
                            App.PhotoCollection.SelectedPhoto.Label.Color.A,
                            App.PhotoCollection.SelectedPhoto.Label.Color.R,
                            App.PhotoCollection.SelectedPhoto.Label.Color.G,
                            App.PhotoCollection.SelectedPhoto.Label.Color.B)
                        );
                }
                else
                {
                    return (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF707070"));
                }
            }
            else
            {
                return (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF707070"));
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //Debugger.Break();
            return value;
        }
    }
}
