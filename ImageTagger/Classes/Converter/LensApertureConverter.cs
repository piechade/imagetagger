using System;
using System.Globalization;
using System.Windows.Data;

namespace ImageTagger
{
    /// <summary>
    ///     Converts a lens aperture from a decimal into a human-preferred string (e.g. 1.8 becomes F1.8)
    /// </summary>
    public class LensApertureConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                return $"F{value:##.0}";
            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            if (!string.IsNullOrEmpty((string) value))
            {
                return decimal.Parse(((string) value).Substring(1));
            }
            return null;
        }
    }
}