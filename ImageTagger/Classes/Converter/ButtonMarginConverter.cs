﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ImageTagger
{
    class ButtonMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (App.PhotoCollection.SelectedPhoto.Label != null)
            {
                if (App.PhotoCollection.SelectedPhoto.Label.Name.Equals(value))
                {
                    return new Thickness(5, 3, 0, 0);
                }
                else
                {
                    return new Thickness(5, 5, 0, 0);
                }
            }
            else
            {
                return new Thickness(5, 5, 0, 0);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
