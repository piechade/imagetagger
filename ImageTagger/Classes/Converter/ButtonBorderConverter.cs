﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace ImageTagger
{
    class ButtonBorderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (App.PhotoCollection.SelectedPhoto.Label != null)
            {
                if (App.PhotoCollection.SelectedPhoto.Label.Name.Equals(value))
                {
                    return new Thickness(2, 2, 2, 4);
                }
                else
                {
                    return new Thickness(1, 1, 1, 1);
                }
            }
            else
            {
                return new Thickness(1, 1, 1, 1);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //Debugger.Break();
            return value;
        }
    }
}
