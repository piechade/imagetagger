﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ImageTagger
{
    public class SimpleTreeLabel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
