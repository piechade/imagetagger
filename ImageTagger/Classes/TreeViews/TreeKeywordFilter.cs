﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace ImageTagger
{
    public class TreeKeywordFilter : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public TreeKeywordFilter()
        {
            this.Children = new ObservableCollection<TreeKeywordFilter>();
        }

        public string Name { get; set; }
        public string Id { get; set; }
        public int Count { get; set; }
        private ObservableCollection<TreeKeywordFilter> children;

        public ObservableCollection<TreeKeywordFilter> Children
        {
            get
            {
                return children;
            }
            set
            {
                children = value;
                OnPropertyChanged("Children");
            }
        }


        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}