﻿using System.Collections.ObjectModel;

namespace ImageTagger
{
    public class TreeKeyword
    {
        public TreeKeyword()
        {
            this.Children = new ObservableCollection<TreeKeyword>();
        }

        public string Name { get; set; }
        public string Id { get; set; }
        public bool IsChecked { get; set; }
        public virtual ObservableCollection<TreeKeyword> Children { get; set; }
    }
}