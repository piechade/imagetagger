﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows.Data;

namespace ImageTagger
{
    public class TreeFilterList : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;


        public string Name { get; set; }
        private ObservableCollection<object> filterObjects;
        private object _filterObjectsLock = new object();

        private SynchronizationContext synchronizationContext = SynchronizationContext.Current;

        public TreeFilterList()
        {
            BindingOperations.EnableCollectionSynchronization(filterObjects, _filterObjectsLock);
        }

        public TreeFilterList(string name)
        {
            Name = name;
            FilterObjects = new ObservableCollection<object>();
        }

        public ObservableCollection<object> FilterObjects
        {
            get
            {
                return filterObjects;
            }
            set
            {
                if (SynchronizationContext.Current == synchronizationContext)
                {
                    filterObjects = value;
                    OnPropertyChanged("FilterObjects");
                }
            }
        }

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
