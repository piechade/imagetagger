﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace ImageTagger
{
    internal class MyStorage
    {
        internal static void StorageXML<T>(string file, T data)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            using (FileStream stream = new FileStream(file, FileMode.Create))
            {
                serializer.Serialize(stream, data);
            }
        }

        internal static T ReadXML<T>(string file)
        {
            try
            {
                using (FileStream stream = new FileStream(file, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    return (T)serializer.Deserialize(stream);
                }
            }
            catch //(Exception)
            {

                return default(T);
            }
        }
    }
}