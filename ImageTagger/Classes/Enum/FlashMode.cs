namespace ImageTagger
{
    public enum FlashMode
    {
        FlashFired,
        FlashDidNotFire
    }
}