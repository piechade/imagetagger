﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Threading;

namespace ImageTagger
{
    [ContentProperty("Items")]
    public class PhotoCollection : INotifyPropertyChanged
    {
        Random random = new Random();

        public ObservableCollection<Photo> Photos { get; set; }
        private object _photosLock = new object();
        private ObservableCollection<Photo> photosView;
        private ObservableCollection<TreeFilterList> treeFilterList;
        private object _treeFilterListsLock = new object();
        private ObservableCollection<TreeKeyword> treeKeywordList;
        private object _treeKeywordListLock = new object();

        public ObservableCollection<Keyword> Keywords { get; set; }
        public ObservableCollection<Label> Labels { get; set; }

        public ObservableCollection<CameraModel> CameraModels { get; set; }
        public ObservableCollection<PhotoCreated> PhotoCreatedList { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        private List<string> mSupportedExtensions;
        [System.Xml.Serialization.XmlIgnore]
        private Photo selectedPhoto;
        [System.Xml.Serialization.XmlIgnore]
        private int loadingValue;

        public string UserControlToShow { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Photo> PhotosView
        {
            get
            {
                return photosView;
            }
            set
            {
                photosView = value;
                OnPropertyChanged("PhotosView");
            }
        }

        public Photo SelectedPhoto
        {
            get
            {
                return selectedPhoto;
            }
            set
            {
                selectedPhoto = value;
                UpdateKeyword(selectedPhoto);
                OnPropertyChanged("SelectedPhoto");
            }
        }

        public ObservableCollection<TreeFilterList> TreeFilterList
        {
            get
            {
                return treeFilterList;
            }
            set
            {
                treeFilterList = value;
                OnPropertyChanged("TreeFilterList");
            }
        }

        public ObservableCollection<TreeKeyword> TreeKeywordList
        {
            get
            {
                return treeKeywordList;
            }
            set
            {
                treeKeywordList = value;
                OnPropertyChanged("TreeKeywordList");
            }
        }

        public int LoadingValue
        {
            get
            {
                return loadingValue;
            }
            set
            {
                loadingValue = value;
                OnPropertyChanged("LoadingValue");
            }
        }

        public PhotoCollection()
        {
            Photos = new ObservableCollection<Photo>();
            Keywords = new ObservableCollection<Keyword>();
            Labels = new ObservableCollection<Label>();
            CameraModels = new ObservableCollection<CameraModel>();
            PhotoCreatedList = new ObservableCollection<PhotoCreated>();
            PhotosView = Photos;

            UserControlToShow = "MyPhotoGrid";

            InitFilter();

            BindingOperations.EnableCollectionSynchronization(Photos, _photosLock);
            BindingOperations.EnableCollectionSynchronization(treeFilterList, _treeFilterListsLock);
            BindingOperations.EnableCollectionSynchronization(treeKeywordList, _treeKeywordListLock);

            mSupportedExtensions = new List<string>()
            { ".bmp", ".jpg", ".gif", ".jpe", ".jpeg", ".jfif", ".tif", ".tiff", ".png" };
        }

        private void AddImage(FileInfo fileInfo)
        {
            Photo Photo = new Photo(fileInfo.FullName);
            var x = Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(async () =>
            {
                Photo.Metadata = new MetaData(Photo.Source);
                Photo.Metadata.Init();
                await Task.Delay(100);
            }));

            x.Completed += (s, e) =>
            {
                //Debug.Print(x.ToString());

                //int random = this.random.Next(0, 3);
                //if (random == 0)
                //{
                //    Keywords[0].ImageList.Add(Photo.MD5Hash);
                //    Labels[0].ImageList.Add(Photo.MD5Hash);
                //    Photo.Label = Labels[0];
                //}

                //if (random == 1)
                //{
                //    Keywords[0].Children[0].ImageList.Add(Photo.MD5Hash);
                //    Labels[1].ImageList.Add(Photo.MD5Hash);
                //    Photo.Label = Labels[1];
                //}


                //if (random == 2)
                //{
                //    Keywords[1].ImageList.Add(Photo.MD5Hash);
                //    Labels[2].ImageList.Add(Photo.MD5Hash);
                //    Photo.Label = Labels[2];
                //}

                PhotoCreated tempPhotoCreated = PhotoCreatedList.FirstOrDefault(a => a.Name.Contains(Photo.Metadata.DateImageTaken.ToShortDateString()));
                if (tempPhotoCreated == null)
                {
                    PhotoCreated temp = new PhotoCreated() { Name = Photo.Metadata.DateImageTaken.ToShortDateString() };
                    temp.ImageList.Add(Photo.MD5Hash);
                    PhotoCreatedList.Add(temp);
                }
                else
                {
                    tempPhotoCreated.ImageList.Add(Photo.MD5Hash);
                }

                CameraModel tempCameraModel = CameraModels.FirstOrDefault(a => a.Name.Contains(Photo.Metadata.CameraModel));
                if (tempCameraModel == null)
                {
                    CameraModel temp = new CameraModel() { Name = Photo.Metadata.CameraModel };
                    temp.ImageList.Add(Photo.MD5Hash);
                    CameraModels.Add(temp);
                }
                else
                {
                    tempCameraModel.ImageList.Add(Photo.MD5Hash);
                }

                Photos.Add(Photo);
            };
        }

        private bool AllowAddingImage(FileInfo fileInfo)
        {
            bool value = false;
            if (Photos.Any(x => x.Source.OriginalString == fileInfo.FullName))
            {
                value = false;
            }
            else if (IsImage(fileInfo.Extension))
            {
                value = true;
            }

            return value;
        }

        private bool IsImage(string extension)
        {
            if (mSupportedExtensions.Contains(extension.ToLower()))
                return true;
            return false;
        }

        public void AddPhotos(List<FileInfo> folder)
        {
            for (int i = 0; i < folder.Count; i++)
            {
                if (AllowAddingImage(folder[i]))
                {
                    AddImage(folder[i]);
                    Thread.Sleep(300);
                }
                LoadingValue = (int)Math.Round((double)(100 * i) / folder.Count());
            }
        }

        public Label GetLabelById(string id)
        {
            for (int i = 0; i < Labels.Count; i++)
            {
                if (Labels[i].Id == id)
                {
                    return Labels[i];
                }
            }

            return null;
        }

        public ObservableCollection<Keyword> GetKeywordById(string id)
        {
            ObservableCollection<Keyword> test = null;
            for (int i = 0; i < Keywords.Count; i++)
            {
                if (Keywords[i].Id == id)
                {
                    test = new ObservableCollection<Keyword> { Keywords[i] };
                }
                var ReturnedKeyword = GetKeywordByIdRecursive(Keywords[i], id);
                if (ReturnedKeyword != null)
                {
                    test = new ObservableCollection<Keyword> { ReturnedKeyword, Keywords[i] };
                }
            }
            return test;
        }

        private Keyword GetKeywordByIdRecursive(Keyword keyword, string id)
        {
            Keyword test = null;
            for (int i = 0; i < keyword.Children.Count; i++)
            {
                if (keyword.Children[i].Id == id)
                {
                    test = keyword.Children[i];
                }

                var ReturnedKeyword = GetKeywordByIdRecursive(keyword.Children[i], id);
                if (ReturnedKeyword != null)
                {
                    test = ReturnedKeyword;
                }
            }
            return test;
        }

        //private Label GetLabel(string v)
        //{
        //    foreach (var item in Labels)
        //    {
        //        var test = item.ImageList.FirstOrDefault(x => x.Contains(v));
        //        if (test != null)
        //        {
        //            return item;
        //        }
        //    }

        //    return new Label();
        //}

        public PhotoCreated GetPhotoCreatedById(string id)
        {
            for (int i = 0; i < PhotoCreatedList.Count; i++)
            {
                if (PhotoCreatedList[i].Id == id)
                {
                    return PhotoCreatedList[i];
                }
            }

            return null;
        }

        public CameraModel GetCameraModelById(string id)
        {
            for (int i = 0; i < CameraModels.Count; i++)
            {
                if (CameraModels[i].Id == id)
                {
                    return CameraModels[i];
                }
            }

            return null;
        }

        public void InitFilter()
        {
            TreeFilterList = new ObservableCollection<TreeFilterList>();
            TreeFilterList.Add(new TreeFilterList("Lables"));
            TreeFilterList.Add(new TreeFilterList("Keywords"));
            TreeFilterList.Add(new TreeFilterList("Data Created"));
            TreeFilterList.Add(new TreeFilterList("Model"));

            TreeKeywordList = new ObservableCollection<TreeKeyword>();
        }

        public void UpdateFilter()
        {
            InitFilter();
            UpdateKeyword(selectedPhoto);
            AddLabelFilter();
            AddKeywordFilter();
            AddCameraModelFilter();
            AddPhotoCreatedFilter();
        }

        public void UpdateLabel(Label label)
        {
            var CheckUpdate = SelectedPhoto.Label != label;

            if (SelectedPhoto.Label != null)
            {
                SelectedPhoto.Label.ImageList.Remove(SelectedPhoto.MD5Hash);
                SelectedPhoto.Label = null;
            }

            if (CheckUpdate)
            {
                SelectedPhoto.Label = label;
                SelectedPhoto.Label.ImageList.Add(SelectedPhoto.MD5Hash);
            }
            InitFilter();
            UpdateFilter();
        }

        private void AddPhotoCreatedFilter()
        {
            foreach (var item in PhotoCreatedList)
            {
                var simpleTreeLabel = TreeFilterList[2].FilterObjects.FirstOrDefault(x => ((SimpleTreeLabel)x).Id.Contains(item.Id));
                if (simpleTreeLabel == null)
                {
                    var label = new SimpleTreeLabel() { Name = item.Name, Id = item.Id, Count = item.ImageList.Count };
                    TreeFilterList[2].FilterObjects.Add(label);
                }
            }
        }

        private void AddCameraModelFilter()
        {
            foreach (var item in CameraModels)
            {
                var simpleTreeLabel = TreeFilterList[3].FilterObjects.FirstOrDefault(x => ((SimpleTreeLabel)x).Id.Contains(item.Id));
                if (simpleTreeLabel == null)
                {
                    var label = new SimpleTreeLabel() { Name = item.Name, Id = item.Id, Count = item.ImageList.Count };
                    TreeFilterList[3].FilterObjects.Add(label);
                }
            }
        }

        private void AddLabelFilter()
        {
            foreach (var item in Labels)
            {
                var test = TreeFilterList[0].FilterObjects.FirstOrDefault(x => ((TreeLabel)x).Id.Contains(item.Id));
                if (test == null)
                {
                    var label = new TreeLabel() { Name = item.Name, Id = item.Id, Color = item.Color, Count = item.ImageList.Count };
                    TreeFilterList[0].FilterObjects.Add(label);
                }
            }
        }

        private void AddKeywordFilter()
        {
            for (int i = 0; i < Keywords.Count; i++)
            {
                if ((TreeKeywordFilter)TreeFilterList[1].FilterObjects.FirstOrDefault(x => ((TreeKeywordFilter)x).Id.Contains(Keywords[i].Id)) == null)
                {
                    TreeFilterList[1].FilterObjects.Add(new TreeKeywordFilter() { Name = Keywords[i].Name, Id = Keywords[i].Id, Count = Keywords[i].ImageList.Count });
                    AddKeywordFilterRecursive(Keywords[i], (TreeKeywordFilter)TreeFilterList[1].FilterObjects[i]);
                }
            }
        }

        private void AddKeywordFilterRecursive(Keyword keyword, TreeKeywordFilter treeKeyword)
        {
            for (int i = 0; i < keyword.Children.Count; i++)
            {
                if (treeKeyword.Children.FirstOrDefault(x => x.Id.Contains(keyword.Children[i].Id)) == null)
                {
                    treeKeyword.Children.Add(new TreeKeywordFilter() { Name = keyword.Children[i].Name, Id = keyword.Children[i].Id, Count = keyword.Children[i].ImageList.Count });
                    AddKeywordFilterRecursive(keyword.Children[i], treeKeyword.Children[i]);
                }
            }
        }

        private void UpdateKeywordFilterRecursive(Keyword keyword, TreeKeywordFilter treeKeyword)
        {
            for (int i = 0; i < keyword.Children.Count; i++)
            {
                Keyword tempKeyword = keyword.Children[i];
                TreeKeywordFilter tempTreeKeyword = treeKeyword.Children[i];
                if (tempTreeKeyword.Id == tempKeyword.Id)
                {
                    tempTreeKeyword.Count = tempKeyword.ImageList.Count;
                }
                AddKeywordFilterRecursive(tempKeyword, tempTreeKeyword);
            }
        }

        //Left side Keyword update OnClick
        private void UpdateKeyword(Photo value)
        {
            if (value != null)
            {
                var Hash = value.MD5Hash;
                TreeKeywordList = new ObservableCollection<TreeKeyword>();

                for (int i = 0; i < Keywords.Count; i++)
                {
                    TreeKeywordList.Add(new TreeKeyword() { Name = Keywords[i].Name, Id = Keywords[i].Id });
                    UpdateKeywordRecursive(Keywords[i], TreeKeywordList[TreeKeywordList.Count - 1], Hash);
                }
            }
        }

        //Left side Keyword update OnClick Recursive
        private void UpdateKeywordRecursive(Keyword keyword, TreeKeyword treeKeyword, string id)
        {
            if (keyword.ImageList.Where(x => x.Contains(id)).ToList().Count > 0)
            {
                treeKeyword.IsChecked = true;
            }

            for (int i = 0; i < keyword.Children.Count; i++)
            {
                treeKeyword.Children.Add(new TreeKeyword() { Name = keyword.Children[i].Name, Id = keyword.Children[i].Id });
                UpdateKeywordRecursive(keyword.Children[i], treeKeyword.Children[treeKeyword.Children.Count - 1], id);
            }
        }

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
