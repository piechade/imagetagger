﻿using MetadataExtractor;
using MetadataExtractor.Formats.Exif;
using MetadataExtractor.Formats.FileSystem;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows.Media.Imaging;

namespace ImageTagger
{
    public class MetaData
    {
        private Uri source;
        private IEnumerable<Directory> directories;
        private ExifSubIfdDirectory subIfdDirectory;
        private ExifIfd0Directory exifIfd0Directory;
        private BitmapMetadata _metadata;

        private string _fileName;
        private DateTime _dateImageTaken;
        private uint _width;
        private uint _height;
        private double _fileSize;
        private string _cameraModel;
        private FlashMode _flashMode;
        private decimal _focalLength;
        private WhiteBalanceMode _whiteBalanceMode;
        private decimal _lensAperture;
        private decimal _exposureCompensation;
        private decimal _exposureTime;
        private ushort _isoSpeed;

        public MetaData(Uri source)
        {
            this.source = source;
        }

        public bool Init()
        {
            directories = ImageMetadataReader.ReadMetadata(source.OriginalString);
            subIfdDirectory = directories.OfType<ExifSubIfdDirectory>().FirstOrDefault();
            exifIfd0Directory = directories.OfType<ExifIfd0Directory>().FirstOrDefault();
            BitmapFrame frame = BitmapFrame.Create(source);
            _metadata = (BitmapMetadata)frame.Metadata;

            FileName = GetFileName();
            DateImageTaken = GetDateImageTaken();
            Width = GetWith();
            Height = GetHeight();
            FileSize = GetFileSize();
            CameraModel = GetCameraModel();
            FlashMode = GetFlashMode();
            FocalLength = GetFocalLength();
            WhiteBalanceMode = GetWhiteBalanceMode();
            LensAperture = GetLensAperture();
            ExposureCompensation = GetExposureCompensation();
            ExposureTime = GetExposureTime();
            IsoSpeed = GetIsoSpeed();

            //foreach (var directory in directories)
            //    foreach (var tag in directory.Tags)
            //        Console.WriteLine($"{directory.Name} - {tag.Name} = {tag.Description}");


            //foreach (var xmpDirectory in directories.OfType<XmpDirectory>())
            //{
            //    foreach (var property in xmpDirectory.XmpMeta.Properties)
            //    {
            //        Console.WriteLine($"{property.Path}: {property.Value}");
            //    }
            //}
            return true;
        }

        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
            }
        }

        private string GetFileName()
        {
            if (source != null)
                return source.Segments[source.Segments.Length - 1];
            return string.Empty;
        }

        public DateTime DateImageTaken
        {
            get
            {
                return _dateImageTaken;
            }
            set
            {
                _dateImageTaken = value;
            }
        }

        private DateTime GetDateImageTaken()
        {
            var date = exifIfd0Directory?.GetDescription(ExifIfd0Directory.TagDateTime);
            DateTime.TryParseExact(date, "yyyy:MM:dd h:m:s", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime value);
            return value;
        }

        public uint Width
        {
            get
            {
                return _width;
            }
            set
            {
                _width = value;
            }
        }

        private uint GetWith()
        {
            var val = QueryMetadata("/app1/ifd/exif/subifd:{uint=40962}");
            if (val == null)
            {
                return 0;
            }
            if (val.GetType() == typeof(uint))
            {
                return (uint)val;
            }
            return Convert.ToUInt32(val);
        }

        public uint Height
        {
            get
            {
                return _height;
            }
            set
            {
                _height = value;
            }
        }

        private uint GetHeight()
        {
            var val = QueryMetadata("/app1/ifd/exif/subifd:{uint=40963}");
            if (val == null)
            {
                return 0;
            }
            if (val.GetType() == typeof(uint))
            {
                return (uint)val;
            }
            return Convert.ToUInt32(val);
        }

        public double FileSize
        {
            get
            {
                return _fileSize;
            }
            set
            {
                _fileSize = value;
            }
        }

        private double GetFileSize()
        {
            var subIfdDirectory = directories.OfType<FileMetadataDirectory>().FirstOrDefault();
            subIfdDirectory.TryGetInt64(FileMetadataDirectory.TagFileSize, out long size);
            return size;
        }

        public string CameraModel
        {
            get
            {
                return _cameraModel;
            }
            set
            {
                _cameraModel = value;
            }
        }

        private string GetCameraModel()
        {
            var val = exifIfd0Directory?.GetDescription(ExifIfd0Directory.TagModel);
            return (val != null ? (string)val : string.Empty);
        }

        public FlashMode FlashMode
        {
            get
            {
                return _flashMode;
            }
            set
            {
                _flashMode = value;
            }
        }

        private FlashMode GetFlashMode()
        {
            if ((ushort?)QueryMetadata("/app1/ifd/exif/subifd:{uint=37385}") % 2 == 1)
                return FlashMode.FlashFired;
            return FlashMode.FlashDidNotFire;
        }

        public decimal FocalLength
        {
            get
            {
                return _focalLength;
            }
            set
            {
                _focalLength = value;
            }
        }

        private decimal GetFocalLength()
        {
            //if (!Directory.TryGetInt32(ExifDirectoryBase.Tag35MMFilmEquivFocalLength, out int value))
            //    return null;
            //return value == 0 ? "Unknown" : GetFocalLengthDescription(value);



            var val = QueryMetadata("/app1/ifd/exif/subifd:{uint=37386}");
            if (val != null)
            {
                return ParseUnsignedRational((ulong)val);
            }
            return 0;
        }

        public WhiteBalanceMode WhiteBalanceMode
        {
            get
            {
                return _whiteBalanceMode;
            }
            set
            {
                _whiteBalanceMode = value;
            }
        }

        private WhiteBalanceMode GetWhiteBalanceMode()
        {
            var mode = (ushort?)QueryMetadata("/app1/ifd/exif/subifd:{uint=37384}");

            if (mode == null)
            {
                return WhiteBalanceMode.Unknown;
            }
            switch ((int)mode)
            {
                case 1:
                    return WhiteBalanceMode.Daylight;
                case 2:
                    return WhiteBalanceMode.Fluorescent;
                case 3:
                    return WhiteBalanceMode.Tungsten;
                case 10:
                    return WhiteBalanceMode.Flash;
                case 17:
                    return WhiteBalanceMode.StandardLightA;
                case 18:
                    return WhiteBalanceMode.StandardLightB;
                case 19:
                    return WhiteBalanceMode.StandardLightC;
                case 20:
                    return WhiteBalanceMode.D55;
                case 21:
                    return WhiteBalanceMode.D65;
                case 22:
                    return WhiteBalanceMode.D75;
                case 255:
                    return WhiteBalanceMode.Other;
                default:
                    return WhiteBalanceMode.Unknown;
            }
        }

        public decimal LensAperture
        {
            get
            {
                return _lensAperture;
            }
            set
            {
                _lensAperture = value;
            }
        }

        private decimal GetLensAperture()
        {
            var val = QueryMetadata("/app1/ifd/exif/subifd:{uint=33437}");
            if (val != null)
            {
                return ParseUnsignedRational((ulong)val);
            }
            return 0;
        }

        public decimal ExposureCompensation
        {
            get
            {
                return _exposureCompensation;
            }
            set
            {
                _exposureCompensation = value;
            }
        }

        private decimal GetExposureCompensation()
        {
            var val = QueryMetadata("/app1/ifd/exif/subifd:{uint=37380}");
            if (val != null)
            {
                return ParseSignedRational((long)val);
            }
            return 0;
        }

        public decimal ExposureTime
        {
            get
            {
                return _exposureTime;
            }
            set
            {
                _exposureTime = value;
            }
        }

        private decimal GetExposureTime()
        {
            var val = QueryMetadata("/app1/ifd/exif/subifd:{uint=33434}");
            if (val != null)
            {
                return ParseUnsignedRational((ulong)val);
            }
            return 0;
        }

        public ushort IsoSpeed
        {
            get
            {
                return _isoSpeed;
            }
            set
            {
                _isoSpeed = value;
            }
        }

        private ushort GetIsoSpeed()
        {
            return (ushort)QueryMetadata("/app1/ifd/exif/subifd:{uint=34855}");
        }

        private decimal ParseUnsignedRational(ulong exifValue) => (exifValue & 0xFFFFFFFFL) / (decimal)((exifValue & 0xFFFFFFFF00000000L) >> 32);

        private decimal ParseSignedRational(long exifValue) => (exifValue & 0xFFFFFFFFL) / (decimal)((exifValue & 0x7FFFFFFF00000000L) >> 32);

        private object QueryMetadata(string query)
        {
            try
            {
                if (_metadata.ContainsQuery(query))
                    return _metadata.GetQuery(query);
                return null;

            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
        }
    }
}