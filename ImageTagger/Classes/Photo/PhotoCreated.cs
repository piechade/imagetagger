﻿using System;
using System.Collections.ObjectModel;

namespace ImageTagger
{
    public class PhotoCreated
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ObservableCollection<string> ImageList { get; set; }

        public PhotoCreated()
        {
            ImageList = new ObservableCollection<string>();
            Id = Guid.NewGuid().ToString("N");
        }
    }
}