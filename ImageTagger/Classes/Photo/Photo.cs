using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace ImageTagger
{
    /// <summary>
    ///     This class describes a single photo - its location, the image and
    ///     the metadata extracted from the image.
    /// </summary>
    public class Photo : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [System.Xml.Serialization.XmlIgnore]
        public Uri Source { get; set; }
        //[System.Xml.Serialization.XmlIgnore]
        //public BitmapFrame Image { get; set; }
        //[System.Xml.Serialization.XmlIgnore]
        //public BitmapSource Thumbnail { get; set; }
        [System.Xml.Serialization.XmlIgnore]
        public MetaData Metadata { get; set; }
        private Label label;

        public Label Label
        {
            get
            {
                return label;
            }
            set
            {
                label = value;
                OnPropertyChanged("Label");
            }
        }

        public Photo()
        {

        }

        public Photo(string path)
        {
            Source = new Uri(path);
        }

        public string MD5Hash
        {
            get
            {
                using (var md5 = System.Security.Cryptography.MD5.Create())
                {
                    using (var stream = File.OpenRead(Source.OriginalString))
                    {
                        return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
                    }
                }
            }
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}