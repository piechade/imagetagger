﻿using System;
using System.Collections.ObjectModel;

namespace ImageTagger
{
    public class Keyword
    {
        public Keyword()
        {
            Children = new ObservableCollection<Keyword>();
            ImageList = new ObservableCollection<string>();
            Id = Guid.NewGuid().ToString("N");
        }

        public string Name { get; set; }
        public string Id { get; set; }
        public ObservableCollection<string> ImageList { get; set; }
        public ObservableCollection<Keyword> Children { get; set; }
    }
}