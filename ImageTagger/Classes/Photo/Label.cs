﻿using System;
using System.Collections.ObjectModel;
using System.Drawing;

namespace ImageTagger
{
    public class Label
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Color Color { get; set; }
        public ObservableCollection<string> ImageList { get; set; }

        public Label()
        {
            ImageList = new ObservableCollection<string>();
            Id = Guid.NewGuid().ToString("N");
        }
    }
}