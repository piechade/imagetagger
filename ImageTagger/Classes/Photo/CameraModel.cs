﻿using System;
using System.Collections.ObjectModel;
using System.Drawing;

namespace ImageTagger
{
    public class CameraModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ObservableCollection<string> ImageList { get; set; }

        public CameraModel()
        {
            ImageList = new ObservableCollection<string>();
            Id = Guid.NewGuid().ToString("N");
        }
    }
}