﻿using System;

namespace ImageTagger
{
    public class ContextItem
    {
        public string Icon { get; set; }
        public string Value { get; set; }
    }
}