﻿using MetadataExtractor;
using MetadataExtractor.Formats.Xmp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows;
using XmpCore;

namespace ImageTagger
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static PhotoCollection PhotoCollection;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //PhotoCollection = MyStorage.ReadXML<PhotoCollection>("photo.xml");
            //PhotoCollection.LoadingValue = 0;
            //PhotoCollection.SelectedPhoto = null;
            //PhotoCollection.Photos = new ObservableCollection<Photo>();
            //PhotoCollection.TreeFilterList = new ObservableCollection<TreeFilterList>
            //{
            //    new TreeFilterList("Lables"),
            //    new TreeFilterList("Keywords"),
            //    new TreeFilterList("Data Created"),
            //    new TreeFilterList("Model")
            //};

            PhotoCollection = new PhotoCollection();

            Keyword keyword1 = new Keyword() { Name = "Test1" };
            Keyword keyword2 = new Keyword() { Name = "Test2" };
            keyword1.Children.Add(keyword2);
            Keyword keyword3 = new Keyword() { Name = "Test3" };

            PhotoCollection.Keywords.Add(keyword1);
            PhotoCollection.Keywords.Add(keyword3);

            Label newLabel1 = new Label() { Name = "Photoalbum", Color = Color.Green };
            Label newLabel2 = new Label() { Name = "Digital", Color = Color.Yellow };
            Label newLabel3 = new Label() { Name = "Delete", Color = Color.Red };
            PhotoCollection.Labels.Add(newLabel1);
            PhotoCollection.Labels.Add(newLabel2);
            PhotoCollection.Labels.Add(newLabel3);
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            if(ImageTagger.Properties.Settings.Default.WindowState == WindowState.Minimized)
            {
                ImageTagger.Properties.Settings.Default.WindowState = WindowState.Normal;
            }

            ImageTagger.Properties.Settings.Default.Save();

            //MyStorage.StorageXML<PhotoCollection>("photo.xml", PhotoCollection);
        }
    }
}

