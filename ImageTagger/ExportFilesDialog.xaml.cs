﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImageTagger
{
    /// <summary>
    /// Interaction logic for ExportFilesDialog.xaml
    /// </summary>
    public partial class ExportFilesDialog : Window
    {
        public ExportFilesDialog()
        {
            InitializeComponent();
            icLabels.ItemsSource = App.PhotoCollection.Labels;
            Labels = new ObservableCollection<Label>();
        }

        private void Button_Dialog_OK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Button_Browse_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog
            {
                IsFolderPicker = true
            };

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                txtPath.Text = dialog.FileName;
            }
        }

        public string Path
        {
            get
            {
                return txtPath.Text;
            }
        }

        public ObservableCollection<Label> Labels { get; set; }

        private void CheckBox_Label_Checked(object sender, RoutedEventArgs e)
        {
            Labels.Add((Label)((CheckBox)sender).DataContext);
        }

        private void CheckBox_Label_Unchecked(object sender, RoutedEventArgs e)
        {
            Labels.Remove((Label)((CheckBox)sender).DataContext);
        }
    }
}
