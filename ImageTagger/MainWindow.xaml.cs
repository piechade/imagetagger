﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace ImageTagger
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private PhotoCollection PhotoCollection = App.PhotoCollection;

        public MainWindow()
        {
            InitializeComponent();

            Main.DataContext = App.PhotoCollection;

            UserControl.Content = new PhotoGrid();

            //TODO make much more checks  
            if (Properties.Settings.Default.WindowTop < 0 && Properties.Settings.Default.WindowLeft < 0)
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }
        }

        private void BtnOpen_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog
            {
                IsFolderPicker = true
            };

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                ReadPhoto(dialog.FileName);
            }
        }

        private void ReadPhoto(string path)
        {
            PhotoCollection.LoadingValue = 0;
            var task = Task.Factory.StartNew(() =>
            {
                List<FileInfo> folder = new DirectoryInfo(path).GetFiles("*", SearchOption.AllDirectories).ToList();
                PhotoCollection.AddPhotos(folder);
                PhotoCollection.LoadingValue = (int)Math.Round((double)(100 * folder.Count) / folder.Count());
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.DataBind, new Action(() =>
                {
                    PhotoCollection.UpdateFilter();
                }));
                PhotoCollection.LoadingValue = -1;
            });

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var test = Tv_Label.ItemsSource = PhotoCollection.TreeFilterList;
        }

        private void KeywordCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            CheckBox CheckBox = ((CheckBox)sender);
            TreeKeyword ClickedKeyword = (TreeKeyword)CheckBox.DataContext;

            Keyword Keyword = PhotoCollection.GetKeywordById(ClickedKeyword.Id)[0];

            if (CheckBox.IsChecked == true)
            {
                Keyword.ImageList.Add(App.PhotoCollection.SelectedPhoto.MD5Hash);
            }
            else
            {
                foreach (var item in Keyword.ImageList.Where(x =>
                    x.Contains(App.PhotoCollection.SelectedPhoto.MD5Hash)).ToList())
                {
                    Keyword.ImageList.Remove(item);
                }
            }

            PhotoCollection.UpdateFilter();
        }

        private void Tv_Label_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeView treeView = (TreeView)sender;
            //treeView.Background = Brush;
            PhotoCollection.PhotosView = new ObservableCollection<Photo>();
            PhotoCollection.PhotosView = PhotoCollection.Photos;

            var value = e.NewValue;
            if (value == null)
            {
                value = e.OldValue;
            }

            if (value != null)
            {
                if (value.GetType() == typeof(TreeLabel))
                {
                    TreeLabel treeLabel = (TreeLabel)value;
                    Label label = PhotoCollection.GetLabelById(treeLabel.Id);
                    PhotoCollection.PhotosView = new ObservableCollection<Photo>();

                    foreach (var item in label.ImageList)
                    {
                        PhotoCollection.PhotosView.Add(PhotoCollection.Photos.FirstOrDefault(x => x.MD5Hash.Contains(item)));
                    }
                }

                if (value.GetType() == typeof(TreeKeywordFilter))
                {
                    TreeKeywordFilter treeKeyword = (TreeKeywordFilter)value;
                    ObservableCollection<Keyword> label = PhotoCollection.GetKeywordById(treeKeyword.Id);
                    PhotoCollection.PhotosView = new ObservableCollection<Photo>();

                    foreach (var item in label[0].ImageList)
                    {
                        PhotoCollection.PhotosView.Add(PhotoCollection.Photos.FirstOrDefault(x => x.MD5Hash.Contains(item)));
                    }
                }

                if (value.GetType() == typeof(SimpleTreeLabel))
                {
                    SimpleTreeLabel treeLabel = (SimpleTreeLabel)value;
                    object label = PhotoCollection.GetPhotoCreatedById(treeLabel.Id);
                    if (label == null)
                    {
                        label = PhotoCollection.GetCameraModelById(treeLabel.Id);
                    }
                    PhotoCollection.PhotosView = new ObservableCollection<Photo>();

                    if (label.GetType() == typeof(PhotoCreated))
                    {
                        foreach (var item in ((PhotoCreated)label).ImageList)
                        {
                            PhotoCollection.PhotosView.Add(PhotoCollection.Photos.FirstOrDefault(x => x.MD5Hash.Contains(item)));
                        }
                    }

                    if (label.GetType() == typeof(CameraModel))
                    {
                        foreach (var item in ((CameraModel)label).ImageList)
                        {
                            PhotoCollection.PhotosView.Add(PhotoCollection.Photos.FirstOrDefault(x => x.MD5Hash.Contains(item)));
                        }
                    }
                }
                if (PhotoCollection.PhotosView.Count - 1 >= 0)
                {
                    PhotoCollection.SelectedPhoto = PhotoCollection.PhotosView[PhotoCollection.PhotosView.Count - 1];
                }

                App.PhotoCollection.InitFilter();
                PhotoCollection.UpdateFilter();
            }
        }

        private void Main_Loaded(object sender, RoutedEventArgs e)
        {
            ReadPhoto(@"C:\Users\Dennis\Desktop\Portugal 2017\test");
        }

        private void AddKeyword_Click(object sender, RoutedEventArgs e)
        {
            var Item = ((MenuItem)e.Source).DataContext;
            ObservableCollection<Keyword> update = PhotoCollection.Keywords;
            if (Item.GetType() == typeof(TreeKeyword))
            {
                Keyword keyword = PhotoCollection.GetKeywordById(((TreeKeyword)Item).Id)[0];

                if (keyword != null)
                {
                    update = keyword.Children;
                }
            }
            InputKeywordDialog inputDialog = new InputKeywordDialog("Enter Keyword Name:", "");
            if (inputDialog.ShowDialog() == true)
                update.Add(new Keyword() { Name = inputDialog.Answer });

            PhotoCollection.UpdateFilter();
        }

        private void EditKeyword_Click(object sender, RoutedEventArgs e)
        {
            TreeKeyword test = ((TreeKeyword)((MenuItem)e.Source).DataContext);
            ObservableCollection<Keyword> keyword = PhotoCollection.GetKeywordById(test.Id);
            InputKeywordDialog inputDialog = new InputKeywordDialog("Edit Keyword Name:", keyword[0].Name);
            if (inputDialog.ShowDialog() == true)
                keyword[0].Name = inputDialog.Answer;
            PhotoCollection.UpdateFilter();
        }

        private void RemoveKeyword_Click(object sender, RoutedEventArgs e)
        {
            TreeKeyword test = ((TreeKeyword)((MenuItem)e.Source).DataContext);
            ObservableCollection<Keyword> keyword = PhotoCollection.GetKeywordById(test.Id);
            if (keyword != null)
            {
                if (keyword[0] != null)
                {
                    if (keyword.Count > 1)
                    {

                        keyword[1].Children.Remove(keyword[0]);
                        PhotoCollection.UpdateFilter();
                    }
                    else
                    {
                        PhotoCollection.Keywords.Remove(keyword[0]);
                    }
                    PhotoCollection.UpdateFilter();
                }
            }
        }

        private void BtTaggingMode_Click(object sender, RoutedEventArgs e)
        {
            var bt = (Button)sender;
            if (UserControl.Content.GetType() == typeof(PhotoGrid))
            {
                UserControl.Content = new TaggingControl();
                bt.Content = "Close Tagging mode";
            }
            else
            {
                UserControl.Content = new PhotoGrid();
                bt.Content = "Tagging mode";
            }
        }

        private void BtExport_Click(object sender, RoutedEventArgs e)
        {
            var lables = PhotoCollection.Labels;

            ExportFilesDialog inputDialog = new ExportFilesDialog();
            if (inputDialog.ShowDialog() == true)
            {
                foreach (var item in inputDialog.Labels)
                {
                    foreach (var item2 in item.ImageList)
                    {
                        Photo photo = PhotoCollection.Photos.FirstOrDefault(x => x.MD5Hash.Contains(item2));
                        string fileName = photo.Source.Segments[photo.Source.Segments.Length - 1];

                        string sourcePath = photo.Source.OriginalString.Replace(fileName, "");
                        string targetPath = Path.Combine(inputDialog.Path, item.Name);

                        // Use Path class to manipulate file and directory paths.
                        string sourceFile = Path.Combine(sourcePath, fileName);
                        string destFile = Path.Combine(targetPath, fileName);

                        // To copy a folder's contents to a new location:
                        // Create a new target folder, if necessary.
                        if (!Directory.Exists(targetPath))
                        {
                            Directory.CreateDirectory(targetPath);
                        }

                        // To copy a file to another location and 
                        // overwrite the destination file if it already exists.
                        File.Copy(sourceFile, destFile, true);
                    }
                }
            }
        }
    }
}

