﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageTagger
{
    /// <summary>
    /// Interaction logic for TaggingControl.xaml
    /// </summary>
    public partial class TaggingControl : UserControl
    {
        public TaggingControl()
        {
            InitializeComponent();
            icLabels.ItemsSource = App.PhotoCollection.Labels;
        }

        private void BtLabel_Click(object sender, RoutedEventArgs e)
        {
            Label label = (Label)((Button)sender).DataContext;
            App.PhotoCollection.UpdateLabel(label);
        }

        private void BtGoLeft_Click(object sender, RoutedEventArgs e)
        {
            int index = App.PhotoCollection.Photos.IndexOf(App.PhotoCollection.SelectedPhoto);
            if (index - 1 >= 0)
            {
                App.PhotoCollection.SelectedPhoto = App.PhotoCollection.Photos[index - 1];
            }
        }

        private void BtGoRight_Click(object sender, RoutedEventArgs e)
        {
            int index = App.PhotoCollection.Photos.IndexOf(App.PhotoCollection.SelectedPhoto);
            if (index + 1 < App.PhotoCollection.Photos.Count)
            {
                App.PhotoCollection.SelectedPhoto = App.PhotoCollection.Photos[index + 1];
            }
        }

        private void LvPhotos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            icLabels.ItemsSource = null;
            icLabels.ItemsSource = App.PhotoCollection.Labels;
        }
    }
}
