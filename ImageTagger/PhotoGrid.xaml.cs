﻿using System.Windows.Controls;
using System.Windows.Input;

namespace ImageTagger
{
    /// <summary>
    /// Interaction logic for PhotoGrid.xaml
    /// </summary>
    public partial class PhotoGrid : UserControl
    {
        private PhotoCollection PhotoCollection = App.PhotoCollection;

        public PhotoGrid()
        {
            InitializeComponent();
        }

        private void TextBlock_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Label label = (Label)((TextBlock)sender).DataContext;
            App.PhotoCollection.UpdateLabel(label);
        }
    }
}
